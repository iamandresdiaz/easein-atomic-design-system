module.exports = {
  title: 'easein.app',
  description: 'Atomic design system',
  dest: 'public',
  base: '/easein-atomic-design-system/',
  port: 8080,
  themeConfig: {
    home: true,
    smoothScroll: true,
    sidebar: [
      {
        title: 'Atoms',
        path: '/atoms/',
        collapsable: false,
        children: [
          {
            title: 'Atoms',
            path: '/atoms/atom/',
            collapsable: true,
            children: [
              ['/atoms/atom/atom', 'atom']
            ]
          }
        ]
      },
      {
        title: 'Molecules',
        path: '/molecules/',
        collapsable: true,
        children: [
          {
            title: 'Molecules',
            path: '/molecules/molecule/',
            collapsable: true,
            children: [
              ['/molecules/molecule/molecule', 'molecule']
            ]
          }
        ]
      },
      {
        title: 'Organisms',
        path: '/organisms/',
        collapsable: true,
        children: [
          {
            title: 'Organisms',
            path: '/organisms/organism/',
            collapsable: true,
            children: [
              ['/organisms/organism/organism', 'organism']
            ]
          }
        ]
      },
      {
        title: 'Templates',
        path: '/templates/',
        collapsable: true,
        children: [
          {
            title: 'Templates',
            path: '/templates/template/',
            collapsable: true,
            children: [
              ['/templates/template/template', 'template']
            ]
          }
        ]
      },
      {
        title: 'Pages',
        path: '/pages/',
        collapsable: true,
        children: [
          {
            title: 'Pages',
            path: '/pages/page/',
            collapsable: true,
            children: [
              ['/pages/page/page', 'page']
            ]
          }
        ]
      },
      {
        title: 'More',
        path: '/more/',
        collapsable: true,
        children: [
          {
            title: 'Contributors',
            path: '/more/contributors/',
            collapsable: true
          }
        ]
      }
    ]
  }
}
