---
home: true
heroText: easein.app
tagline: Atomic design system
actionText: Get Started
actionLink: /contents/
footer: MIT Licensed | Copyright ©2020-present | easein.app
---
